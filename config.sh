#!/bin/bash

BASE_DIR="$HOME/.elixir_test"
COLUMNS=`tput cols`
EBS_NAME="Elixir Build Script (EBS)"
ELIXIR_VERSIONS=(tags/v1.4.1 v1.4 master)
EXPECTED_LOG_TYPES=(all err out)
GIT_URL="git@github.com:elixir-lang/elixir.git"
LOGS_DIR="${BASE_DIR}/logs"
RELEASES_DIR="${BASE_DIR}/releases"
SCRIPT_PATH=$(realpath $0)
SCRIPT_PATH_DIRNAME=$(dirname $SCRIPT_PATH)
SEPARATOR="-"
STATUS_CODE_FILE_NAME=".the_status_code_of_script_build"
TAGS_RELEASES_DIR="${RELEASES_DIR}/tags"
TMP_BASE_DIR="${BASE_DIR}/tmp"
TMP_SOURCE_DIR="${TMP_BASE_DIR}/elixir"
