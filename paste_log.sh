#!/bin/bash

if [ ! $# -eq 2 ]
then
  >&2 echo "Invalid number of arguments."
  >&2 echo "Expected elixir version and log type."
  >&2 echo "When log type is one of: '${EXPECTED_LOG_TYPES[@]}'."
  exit 1
fi

source "$(dirname $0)/config.sh"

LOG_PATH="$LOGS_DIR/${1}/std${2}.log"

if [[ ! " ${EXPECTED_LOG_TYPES[@]} " =~ " ${2} " ]]; then
  (>&2 echo -e "Specified log type: '${2}' is unknown!\nValid values: '${EXPECTED_LOG_TYPES[@]}'.")
  exit 1
fi

if [ ! -f $LOG_PATH ]
then
  (>&2 echo -e "Log file: '${LOG_PATH}' does not exists!\nCheck if it's valid and you already compiled '${1}' version of Elixir.")
  exit 1
fi

echo $(cat $LOG_PATH | curl -F 'sprunge=<-' http://sprunge.us)
