## Elixir Build Scripts (EBS)

Scripts that helps build, testing and reports issues for Elixir language.

### Usage:

1. Clone this repository
```
git clone git@gitlab.com:ex-open-source/ex_build_scripts.git
```
2. Enter `ex_build_scripts` folder:
```
cd ex_build_scripts
```
3. Edit `config.sh` with your favorite editor
```
nano config.sh
```
4. Run main script like:
```
./elixir_build.sh /path/to/main.log
```

### Configuration:
1. `BASE_DIR`<br/>
This is path to directory where script will work on
2. `COLUMNS`<br/>
**NOTE**: do not change it unless you know what you do.<br/>
For debugging output script split some parts by line with specified character.<br/>
Script will use it to set that line width, so line will fit your console width.
3. `EBS_NAME`<br/>
This is just shourtcut to script name, because it's used multiple times.<br/>
Modification of this variable should not have any effect on how this scripts works.
4. `ELIXIR_VERSIONS`<br/>
This is a variable you probably looking for.<br/>
You can specify here what versions you want to test.
5. `EXPECTED_LOG_TYPES`<br/>
**NOTE**: do not change it unless you know what you do.<br/>
This is variable to store prompt choices.
6. `GIT_URL`<br/>
It a git url for [elixir-lang/elixir](https://github.com/elixir-lang/elixir) project.
7. `LOGS_DIR`<br/>
This is base path for all logs (except main log)
8. `RELEASES_DIR`<br/>
Similarry to `LOGS_DIR`, but for releases - where script will build and test Elixir sources
9. `SCRIPT_PATH`<br/>
**NOTE**: Do not change this, please.<br/>
It's path for current script.
10. `SCRIPT_PATH_DIRNAME`<br/>
**NOTE**: Do not change this, please.<br/>
It's directory that contains current script.<br/>
It's used when build or test fail to run second script to paste log.
11. `SEPARATOR`<br/>
**NOTE**: this variable should contain only one character as value.<br/>
For debugging output script split some parts by line with specified character.<br/>
Script will use it to set character that should fill lines.<br/>
12. `STATUS_CODE_FILE_NAME`<br/>
*NOTE**: do not change it unless you know what you do.<br/>
This variable describes file name that will contains status code of build and test result.
13. `TAGS_RELEASES_DIR`<br/>
**NOTE**: Do not change this, please.<br/>
Tags are special case of target to **checkout**.<br/>
This is shourtcut for script to create directory if there is at least one target that is tag.
14. `TMP_BASE_DIR`<br/>
This is path to directory with temporary data.<br/>
Yout do not need to care about it.
15. `TMP_SOURCE_DIR`<br/>
Script will clone Elixir source only once and then just copy sources to releases dir for current target.<br/>
This will be automatically erased after script ends building and testing code for all selected targets.

### Main log

Main log will contain debug data like file and/or directory copying or deletion that you do not need to see.<br/>
If not set it will output to screen.<br/>
Other logs and prompts (in case build/tests fails) always will be visible in your screen until you redirect stdout output which is not recommend.

### Other logs

This script will save stdout and stderr to separate 3 files (based on configuration in `config.sh`).<br/>
In case you want to see/use only warnings/errors then look at build file called: `stderr.log`.<br/>
In case you want to see/use only build/test messages then look at build file called: `stdout.log`.<br/>
In case you want to see/use all messages then look at build file called: `stdall.log`.<br/>
By default log files would be located at: `/your/home_directory/.elixir_test/logs/branch/name/`.

### FAQ
1. **Q**: Do I need internet connection?<br/>
A: You need internet connection for 3 things: pull this repository, clone Elixir source and (optionally) paste log file(s).
2. **Q**: Do I need to restart script to paste log file after I choose bad type or answer "no"?<br/>
**A**: No! You can use second script that first uses.<br/>
Type: `./paste_log.sh target/version logtype` to automaticall find and paste log file.
3. **Q**: What will happen if I type incorrent answer?<br/>
**A**: In case your answer is empty script will automatically choose first (capitalized) default option.<br/>
In "Yes/no" questions you can answer with just `y` or `n` letters.<br/>
In case your answer does not match previous rules and expected answers script will ask again same question, so don't worry to fail!<br/>
**NOTE**: The script automatically converts large letters on the corresponding smaller, so you do not need to care about sensitivity.
4. **Q**: Where my logs are pasted?<br/>
**A**: This script uses: [sprunge](http://sprunge.us/) service.