#!/bin/bash

if [ $# -gt 1 ]
then
  >&2 echo "Invalid number of arguments."
  >&2 echo -n "Expected none or path to log file "
  >&2 echo "that will contain ouput of this script".
  exit 1
fi

source "$(dirname $0)/config.sh"

LOG_FILE=$1

build_release() {
  local log_dir="${LOGS_DIR}/${1}"
  local stdall="${log_dir}/stdall.log"
  local stderr="${log_dir}/stderr.log"
  local stdout="${log_dir}/stdout.log"
  (((make clean test; echo $? > $STATUS_CODE_FILE_NAME ) | tee $stdout) 3>&1 1>&2 2>&3 | tee $stderr) &> $stdall
  local status=$(cat $STATUS_CODE_FILE_NAME)
  if [ $status -ne 0 ]
  then
    echo "Build failed!"
    yes_no_question "Would you like to send a build log?" answer
    if [[ $answer -eq "yes" ]] || [[ $answer -eq "y" ]]
    then
      one_of_question "Choose a log type:" EXPECTED_LOG_TYPES[@] answer
      url=$("${SCRIPT_PATH_DIRNAME}/paste_log.sh" ${1} ${answer})
      echo "Send '${answer}' log to: '${url}'."
    fi
  fi
  echo "The complete build log for '$1' release is located at: '${stdall}'."
}

build_releases() {
  for elixir_version in ${ELIXIR_VERSIONS[@]}
  do
    local release_dir="${RELEASES_DIR}/${elixir_version}"
    echo "Starting work at '$elixir_version' release!"
    echo "Copying Elixir sources to: '${release_dir}'."
    if [[ -n $LOG_FILE ]]
    then
      prepare_release $elixir_version > $LOG_FILE 2>&1
    else
      prepare_release $elixir_version
    fi
    echo "Compiling and testing Elixir source."
    build_release $elixir_version
    if [[ -n $LOG_FILE ]]
    then
      print_separator_line > $LOG_FILE 2>&1
      print_separator_line
    else
      print_separator_line
    fi
  done
}

clone_source() {
  git clone $GIT_URL $TMP_SOURCE_DIR
}

create_structure() {
  echo "Creating directory structure"
  mkdir -pv $LOGS_DIR
  mkdir -pv $RELEASES_DIR
  mkdir -pv $TMP_BASE_DIR
}

one_of_question() {
  if [[ $# -eq 3 ]]
  then
    local prompt="$1"
    local prompt_answers=("${!2}")
    local expected_answers=("${!2}")
    local passed_variable=$3
  elif [[ $# -eq 4 ]]
  then
    local prompt="$1"
    local prompt_answers=("${!2}")
    local expected_answers=("${!3}")
    local passed_variable=$4
  fi
  local expected_answers_string=${expected_answers[@],,}
  local prompt_answers_string=${prompt_answers[@],,}
  prompt_answers_string="$(tr '[:lower:]' '[:upper:]' <<< ${prompt_answers_string:0:1})${prompt_answers_string:1}"
  prompt_answers_string=$(sed "s/ /\//g" <<<"${prompt_answers_string}")
  read -p "$prompt [${prompt_answers_string}] " answer
  answer=${answer,,}
  if [[ -z "${answer}" ]]
  then
    eval $passed_variable="'${expected_answers[0],,}'"
  elif [[ " ${expected_answers_string} " =~ " ${answer,,} " ]]
  then
    eval $passed_variable="'${answer}'"
  else
    if [[ $# -eq 3 ]]
    then
      one_of_question "$1" $2 answer
    elif [[ $# -eq 4 ]]
    then
      one_of_question "$1" $2 $3 answer
    fi
    eval $passed_variable="'${answer}'"
  fi
}

post_cleanup() {
  print_separator_line
  rm -frv $TMP_BASE_DIR
  print_separator_line
}

pre_cleanup() {
  if [ -d $BASE_DIR ]
  then
    echo "Deleting old instancesa at '${BASE_DIR}'"
    rm -frv $BASE_DIR
  fi
}

prepare_release() {
  local log_dir="${LOGS_DIR}/${1}"
  local release_dir="${RELEASES_DIR}/${1}"

  mkdir -pv $log_dir
  if [[ $1 == tags/* && ! -d $TAGS_RELEASES_DIR ]]
  then
    mkdir -pv $TAGS_RELEASES_DIR
  fi

  cp -frv $TMP_SOURCE_DIR $release_dir
  cd $release_dir
  if [ $1 != "master" ]
  then
    git checkout $1
  fi
}

prepare_source() {
  print_separator_line
  print_configuration
  print_separator_line
  pre_cleanup
  print_separator_line
  create_structure
  print_separator_line
  clone_source
}

print_configuration() {
  echo "Configuration:"
  echo "Base dir:                   '${BASE_DIR}'"
  echo "Elixir git url:             '${GIT_URL}'"
  echo "Line separator:             '${SEPARATOR}'"
  echo "Logs dir:                   '${LOGS_DIR}'"
  echo "Releases dir:               '${RELEASES_DIR}'"
  echo "Script log file:            '${SCRIPT_LOG}'"
  echo "Target Elixir versions:     '${ELIXIR_VERSIONS[@]}'"
  echo "Terminal columns:           '${COLUMNS}'"
  echo "Temporary directory:        '${TMP_BASE_DIR}'"
}

print_separator_line() {
  echo
  for i in $(seq $COLUMNS)
  do
    echo -n $SEPARATOR
  done
  echo
  echo
}

start() {
  echo "${EBS_NAME} started!"
  if [[ -n $LOG_FILE ]]
  then
    prepare_source > $LOG_FILE 2>&1
  else
    prepare_source
  fi
  if [[ $? -eq 0 ]]
  then
    if [[ -n $LOG_FILE ]]
    then
      print_separator_line > $LOG_FILE 2>&1
    else
      print_separator_line
    fi
    build_releases
    print_separator_line
    echo "Deleting temporary files."
    if [[ -n $LOG_FILE ]]
    then
      post_cleanup > $LOG_FILE 2>&1
    else
      post_cleanup
    fi
    echo "${EBS_NAME} finished!"
  else
    print_separator_line
    (>&2 echo "${EBS_NAME} failed!")
  fi
}

yes_no_question() {
  local passed_variable=$2
  local prompt_answers=(yes no)
  local expected_answers=(yes y no n)
  one_of_question "$1" prompt_answers[@] expected_answers[@] answer
  eval $passed_variable="'${answer}'"
}

start
